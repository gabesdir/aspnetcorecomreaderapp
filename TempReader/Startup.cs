﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace TempReader
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddComReader("com3");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                if (context.Request.Path.Value.Contains("favicon.ico"))
                {
                    context.Response.StatusCode = StatusCodes.Status404NotFound;
                    await context.Response.WriteAsync("go away");
                }
                else
                {
                    string[] paths = context.Request.Path.Value.Split('/');
                    if (!string.IsNullOrEmpty(paths[1]))
                    {
                        switch (paths[1])
                        {
                            case "ports":
                                var ports = SerialPort.GetPortNames();
                                string response = "";
                                foreach (var p in ports)
                                {
                                    response += $"{p}\n";
                                }
                                await context.Response.WriteAsync(response);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        var value = context.RequestServices.GetRequiredService<ComReader>().SerialData;
                        await context.Response.WriteAsync(value);
                    }
                }
            });
        }
    }
}
