﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using TempReader;

namespace TempReader
{
    public static class ServicecollectionExtension
    {
        public static IServiceCollection AddComReader(this IServiceCollection services, string port)
        {
            services.AddSingleton(sp => 
            {
                var reader = new ComReader(sp.GetRequiredService<ILoggerFactory>(), port);
                reader.BeginRead();
                return reader;
            });

            return services;
        }
    }
}
