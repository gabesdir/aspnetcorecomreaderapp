﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace TempReader
{
    public class ComReader
    {
        private bool IsOpening = false;

        private readonly string portName;
        private string _data = "no data yet";

        public string SerialData {
            get
            {
                if (SerialPort == null)
                {
                    return "error missing port handle";
                }
                if ( !SerialPort.IsOpen)
                {
                    return $"error port closed [{SerialPort.PortName}]";
                }
                else
                {
                    return _data;
                }
            }
        }

        private readonly ILogger logger;
        private SerialPort SerialPort;
        public ComReader(ILoggerFactory loggerFactory, string port)
        {
            this.portName = port;
            this.logger = loggerFactory.CreateLogger("com reader");
            PortCheck();
        }
        public void PrintPorts()
        {
            logger.LogInformation("Serial ports available:");
            foreach (var portName in SerialPort.GetPortNames())
            {
                logger.LogInformation(portName);
            }
        }

        private void PortCheck()
        {
            var pollTask = Task.Run(() => 
            {
                while (true)
                {
                    if (SerialPort != null && !SerialPort.IsOpen)
                    {
                        BeginRead();
                    }
                    Thread.Sleep(500);
                }
            });
        }

        public void BeginRead()
        {
            if (IsOpening)
            {
                return;
            }

            IsOpening = true;
            if (SerialPort != null)
            {
                SerialPort.DataReceived -= SerialPortDataReceived;
                SerialPort.Close();
                SerialPort = null;
            }

            this.SerialPort = new SerialPort(portName)
            {
                BaudRate = 9600,
                Parity = Parity.None,
                StopBits = StopBits.One,
                DataBits = 8,
                Handshake = Handshake.None
            };

            SerialPort.DataReceived += SerialPortDataReceived;
            // Now open the port.
            try
            {
                SerialPort.Open();
            }
            catch (Exception e)
            {
                _data = $"{e.Message} [{portName}]";
            }
            finally
            {
                IsOpening = false;
            }
        }

        private void SerialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var serialPort = (SerialPort)sender;

            // Read the data that's in the serial buffer.
            try
            {
                var data = serialPort.ReadExisting();

                if (string.IsNullOrEmpty(data))
                {
                    string message = $"Ничего не считано из {serialPort.PortName}";
                    logger.LogError(message);
                    _data = message;
                    return;
                }
                else
                {
                    // Write to output.
                    logger.LogInformation($"Чтение данных: {data}");
                    _data = data;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                _data = ex.Message;
            }
        }
    }
}
