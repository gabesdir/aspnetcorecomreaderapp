﻿using System;
using System.IO.Ports;
using System.Linq;

namespace ComRead
{
    public class SerialWrap
    {
        public SerialPort SerialPort;

        public static void PrintPorts()
        {
            Console.WriteLine("Serial ports available:");
            Console.WriteLine("-----------------------");
            foreach (var portName in SerialPort.GetPortNames())
            {
                Console.Write(portName);
            }
        }

        public void BeginRead(string portName)
        {
            this.SerialPort = new SerialPort(portName)
            {
                BaudRate = 9600,
                Parity = Parity.None,
                StopBits = StopBits.One,
                DataBits = 8,
                Handshake = Handshake.None
            };

            SerialPort.DataReceived += SerialPortDataReceived;
            // Now open the port.
            SerialPort.Open();
        }

        private void SerialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var serialPort = (SerialPort)sender;

            // Read the data that's in the serial buffer.
            var serialdata = serialPort.ReadExisting();

            // Write to debug output.
            Console.Write(serialdata);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var port = SerialPort.GetPortNames().Last();
            Console.WriteLine($"Use last com port: {port}");

            new SerialWrap().BeginRead(port);

            Console.ReadKey();
        }
    }
}
